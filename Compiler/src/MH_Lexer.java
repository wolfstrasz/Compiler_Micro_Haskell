

import java.io.* ;

class MH_Lexer extends GenLexer implements LEXER {

    static class VarAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "VAR" ;}
        public int numberOfStates() {return 3 ;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(CharTypes.isSmall(c)) return 1; else return 2;
            case 1: if(CharTypes.isSmall(c) ||
                        CharTypes.isLarge(c) ||
                        CharTypes.isDigit(c) ||
                        c == '\'') return 1; else return 2;
            default: return 2;
            }
        }

        public boolean accepting(int state){return state == 1;}
        public int deadState(){return 2;}
    }

    static class NumAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "NUM" ;}
        public int numberOfStates() {return 4 ;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(c == '0')return 1;
                    else if(CharTypes.isDigit(c))return 2;
                    else return 3;
            case 1: return 3;
            case 2: if(CharTypes.isDigit(c))return 2; else return 3;
            default: return 3;
            }
        }

        public boolean accepting(int state){return (state == 1 || state == 2);}
        public int deadState(){return 3;}
    }

    static class BooleanAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "BOOLEAN" ;}
        public int numberOfStates() {return 9;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(c == 'T')return 1;       // Follow True or False (both end on E)
                    else if (c == 'F')return 2;
                    else return 8;
            case 1: if(c == 'r') return 3; else return 8;
            case 2: if(c == 'a') return 4; else return 8;
            case 3: if(c == 'u') return 5; else return 8;
            case 4: if(c == 'l') return 6; else return 8;
            case 5: if(c == 'e') return 7; else return 8;
            case 6: if(c == 's') return 5; else return 8;
            case 7: return 8;
            default: return 8;
            }
        }

        public boolean accepting(int state){return state==7;}
        public int deadState(){return 8;}
    }

    static class SymAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "SYM" ;}
        public int numberOfStates() {return 3 ;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(CharTypes.isSymbolic(c))return 1; else return 2;
            case 1: if(CharTypes.isSymbolic(c))return 1; else return 2;
            default: return 2;
            }
        }

        public boolean accepting(int state){return state==1;}
        public int deadState(){return 2;}
    }

    static class WhitespaceAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "" ;} // discards the whitespaces
        public int numberOfStates() {return 3 ;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(CharTypes.isWhitespace(c))return 1; else return 2;
            case 1: if(CharTypes.isWhitespace(c))return 1; else return 2;
            default: return 2;
            }
        }

        public boolean accepting(int state){return state==1;}
        public int deadState(){return 2;}
    }

    static class CommentAcceptor extends Acceptor implements DFA {
        // add code here
        public String lexClass() {return "" ;} // discards comments
        public int numberOfStates() {return 5 ;}

        public int nextState(int state, char c)
        {
            switch(state)
            {
            case 0: if(c == '-')return 1; else return 4;
            case 1: if(c == '-')return 2; else return 4;
            case 2: if(c == '-')return 2;
                    else if(!(CharTypes.isNewline(c) || CharTypes.isSymbolic(c)))return 3;
                    else return 4;
            case 3: if(!CharTypes.isNewline(c)) return 3;
                  //  else if(CharTypes.isNewline(c)) return 4;
                    else return 4;
            default: return 4;
            }
        }

        public boolean accepting(int state){return state==4;}
        public int deadState(){return 5;}
    }

    static class TokAcceptor extends Acceptor implements DFA {
        // used to create special tokens e.g. "if"
        String tok ;
        int tokLen ;
        TokAcceptor (String tok) {this.tok = tok ; tokLen = tok.length() ;}

        // add code here
        public String lexClass() {return this.tok;}
        public int numberOfStates() {return tokLen+2 ;} // States for the string chars + accepting + dead


        public int nextState(int state, char c)
        {
            if(state<tokLen)
            {
                if(c == tok.charAt(state)) return state+1;
                else return tokLen+1;
            }
            else return tokLen+1;
        }

        public boolean accepting(int state){return state == tokLen;}
        public int deadState(){return tokLen+1;}
    }

    // add definition of MH_acceptors here
    private static DFA varAcceptor = new VarAcceptor();
    private static DFA numAcceptor = new NumAcceptor();
    private static DFA booleanAcceptor = new BooleanAcceptor();
    private static DFA symAcceptor = new SymAcceptor();
    private static DFA whitespaceAcceptor = new WhitespaceAcceptor();
    private static DFA commentAcceptor = new CommentAcceptor();
    private static DFA integerAcceptor = new TokAcceptor("Integer");
    private static DFA boolAcceptor = new TokAcceptor("Bool");
    private static DFA ifAcceptor = new TokAcceptor("if");
	private static DFA thenAcceptor = new TokAcceptor("then");
	private static DFA elseAcceptor = new TokAcceptor("else");
	private static DFA openBracketAcceptor = new TokAcceptor("(");
	private static DFA closedBracketAcceptor = new TokAcceptor(")");
	private static DFA semicolonAcceptor = new TokAcceptor(";");

    private static DFA[] MH_acceptors = new DFA[] {
        integerAcceptor,
        booleanAcceptor,
        boolAcceptor,
        thenAcceptor,
        elseAcceptor,
        ifAcceptor,
        varAcceptor,
        numAcceptor,
        openBracketAcceptor,
        closedBracketAcceptor,
        semicolonAcceptor,
        whitespaceAcceptor,
        commentAcceptor,
        symAcceptor
    };


    MH_Lexer (Reader reader) {
        super(reader, MH_acceptors) ;
    }

}
