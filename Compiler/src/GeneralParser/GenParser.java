import MyErrors.UnexpectedInput;

import java.util.* ;

public abstract class GenParser implements PARSER {
    public abstract String startSymbol();
    public abstract String[] tableEntry (String nonterm, String tokenType);

    public TREE parseTokenStream (LEXER tokStream) throws Exception {
        return parseTokenStreamAs (tokStream, this.startSymbol()) ;
    }

        public TREE parseTokenStreamAs(LEXER tokStream, String nonterm) throws Exception {
        Stack<TREE> theStack = new Stack<>() ;
        SyntaxTree rootNode = new SyntaxTree(nonterm) ;
        theStack.push(rootNode) ;
        SyntaxTree currNode ;
        String currLabel ;
        Token currToken ;
        String currLexClass ;
        do {
            currNode = (SyntaxTree)(theStack.pop());
            currLabel = currNode.getLabel();
            currToken = tokStream.peekProperToken();
            if (currToken == null) {
                currLexClass = null;
            } else {
                currLexClass = currToken.lexClass();
            }
            if (currNode.isTerminal()) {
                // match expected terminal against input token
                if (currLexClass != null && currLexClass.equals(currLabel)) {
                    // all OK
                    currNode.setValue (currToken.value());
                    tokStream.pullToken();
                } else { // report error: expected terminal not found
                    if (currToken == null) {
                        throw new UnexpectedInput(currLabel, "end of input");
                    } else throw new UnexpectedInput (currLabel, currLexClass);
                }
            } else {
                // lookup expected non-terminal vs input token in table
                // OK if currLexClass is null (end-of-input marker)
                String[] rhs = tableEntry (currLabel, currLexClass);
                if (rhs != null) {
                    SyntaxTree[] children = new SyntaxTree[rhs.length];
                    for (int i=0; i<rhs.length; i++) {
                        children[i] = new SyntaxTree(rhs[i]);
                    }
                    currNode.setRhsChildren(rhs,children);
                    for (int i=rhs.length-1; i>=0; i--) {
                        theStack.push(children[i]);
                    }
                } else if (currToken == null) {
                    throw new UnexpectedInput (currLabel, "end of input");
                } else {
                    // report error: blank entry in table
                    throw new UnexpectedInput (currLabel, currLexClass);
                }
            }
        } while (!theStack.empty());
            Token next = tokStream.pullProperToken();
        if (next != null) {
            // non-fatal warning: parse completed before end of input
            System.out.println ("Warning: " + next.value() +
                    " found after parse completed.");
        } else {
            System.out.println ("Parse successful.");
        }
        return rootNode;
    }

    // Perhaps add method for parsing as a specified nonterminal
}





