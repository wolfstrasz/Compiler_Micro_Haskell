public interface PARSER {
    TREE parseTokenStream (LEXER tokStream) throws Exception ;
    TREE parseTokenStreamAs (LEXER tokStream, String nonterm) throws Exception ;
}