import java.io.*;


public class MH_ParserDemo {

    private static GenParser MH_Parser = new MH_Parser();

    public static void main (String[] args) throws Exception {
            Reader reader = new BufferedReader (new FileReader (args[0])) ;
            LEXER MH_Lexer = new CheckedSymbolLexer(new MH_Lexer (reader)) ;
            TREE theTree = MH_Parser.parseTokenStream (MH_Lexer) ;
            }
}