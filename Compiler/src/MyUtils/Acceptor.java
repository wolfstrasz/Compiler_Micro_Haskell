import MyErrors.StateOutOfRange;

public abstract class Acceptor {

    // Stubs for methods specific to a particular DFA
    public abstract String lexClass() ;
    public abstract int numberOfStates() ;
    public abstract int nextState (int state, char input) ;
    public abstract boolean accepting (int state) ;
    public abstract int deadState () ;

    // DFA machinery
    private int currState = 0 ;
    public void reset () {currState = 0 ;}

    public void processChar (char c) throws StateOutOfRange {
        // performs the state transition
        currState = nextState (currState,c) ;
        if (currState >= numberOfStates() || currState < 0) {
            throw new StateOutOfRange (lexClass(), currState) ;
        }
    }

    public boolean isAccepting () {return accepting (currState) ;}
    public boolean isDead () {return (currState == deadState()) ;}
}
