public class CharTypes {

    static public boolean isLetter (char c) {
        return (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'));
    }

    static public boolean isSmall (char c) {
        return (('a' <= c && c <= 'z') || c == '_');
    }

    static public boolean isLarge (char c) {
        return ('A' <= c && c <= 'Z');
    }

    static public boolean isDigit (char c) {
        return ('0' <= c && c <= '9');
    }

    static public boolean isSymbolic (char c) {
        return (c == '!' || c == '#' || c == '$' || c == '%' || c == '&' ||
                c == '*' || c == '+' || c == '.' || c == '/' || c == '<' ||
                c == '=' || c == '>' || c == '?' || c == '@' || c == '\\' ||
                c == '^' || c == '|' || c == '-' || c == '~' || c == ':');
    }

    static public boolean isWhitespace (char c) {
        return (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\f');
    }

    static public boolean isNewline (char c) {
        return (c == '\r' || c == '\n' || c == '\f' );
    }

}