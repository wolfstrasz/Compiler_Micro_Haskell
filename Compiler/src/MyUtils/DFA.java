import MyErrors.StateOutOfRange;

public interface DFA {
    String lexClass() ;
    int numberOfStates() ;
    void reset() ;
    void processChar (char c) throws StateOutOfRange;
    boolean isAccepting () ;
    boolean isDead () ;
}



