public class SyntaxTree implements TREE {

    private String label ;        // Convention: non-terminals begin with "#".
    private String value ;
    private String[] rhs ;
    private TREE[] children ;

    public String getLabel() {return label ;}
    public boolean isTerminal() {return (label.charAt(0) != '#') ;}
    public String getValue() {return value ;}
    public void setValue(String value) {this.value = value ;}
    public String[] getRhs() {return rhs ;}
    public TREE[] getChildren() {return children ;}
    public void setRhsChildren(String[] rhs, TREE[] children) {
        this.rhs = rhs ; this.children = children ;
    }

    // Constructors
    public SyntaxTree(String label) {this.label = label ;}
}
