import java.io.*;
import MyErrors.*;
public class LexerDemo {

    public static void main (String[] args)
            throws StateOutOfRange, IOException, Exception, LexError {
        BufferedReader consoleReader = new BufferedReader (new InputStreamReader (System.in)) ;
        while (0==0) {
            System.out.print ("Lexer> ") ;
            String inputLine = consoleReader.readLine() ;
            Reader lineReader = new BufferedReader (new StringReader (inputLine)) ;
            GenLexer demoLexer = new MH_Lexer (lineReader) ;
            try {
                Token currTok = demoLexer.pullProperToken() ;
                while (currTok != null) {
                    System.out.println (currTok.value() + " \t" +
                            currTok.lexClass()) ;
                    currTok = demoLexer.pullProperToken() ;
                }
            } catch (Exception x) {
                System.out.println ("Error: " + x.getMessage()) ;
            }
        }
    }
}
