public interface LEXER {
    Token pullToken () throws Exception ;
    // pulls next token from stream, regardless of class
    Token pullProperToken () throws Exception ;
    // pulls next token not of class "" (e.g. skip whitespace and comments)
    Token peekToken () throws Exception ;
    // returns next token without removing it from stream
    Token peekProperToken () throws Exception ;
    // similarly for non-"" tokens
    // All these methods return null once end of input is reached
}