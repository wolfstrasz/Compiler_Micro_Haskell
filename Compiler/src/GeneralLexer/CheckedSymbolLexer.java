import MyErrors.LexError;
import MyErrors.StateOutOfRange;
import MyErrors.UnknownSymbol;

import java.io.* ;

public class CheckedSymbolLexer implements LEXER {

    private GenLexer tokStream ;

    public CheckedSymbolLexer (GenLexer tokStream) {
	this.tokStream = tokStream ;
    }

    private static String[] validTokens = new String[]{"::", "->", "=", "==", "<=", "+", "-"} ;

    private static String checkString (String s) throws UnknownSymbol {
        for (String validToken : validTokens) {
            if (s.equals(validToken)) return s;
        }
	    throw new UnknownSymbol(s) ;
    }

    private static Token checkToken (Token t) throws UnknownSymbol {
        if (t != null && t.lexClass().equals("SYM")) {
            return new Token(t.value(), checkString(t.value())) ;
        } else return t;
    }

    public Token pullToken ()throws LexError, StateOutOfRange, IOException, UnknownSymbol {
	    return checkToken (tokStream.pullToken());
    }

    public Token pullProperToken () throws LexError, StateOutOfRange, IOException, UnknownSymbol {
	    return checkToken (tokStream.pullProperToken());
    }

    public Token peekToken () throws LexError, StateOutOfRange, IOException, UnknownSymbol {
	    return checkToken (tokStream.peekToken());
    }

    public Token peekProperToken () throws LexError, StateOutOfRange, IOException, UnknownSymbol {
	    return checkToken (tokStream.peekProperToken());
    }

}



