import MyErrors.LexError;
import MyErrors.StateOutOfRange;

import java.io.* ;

// Implementation of longest-match lexer.
// We go for simplicity and clarity rather than maximum efficiency.

public class GenLexer implements LEXER {

    private static final char EOF = (char)65535 ;
	// for reading characters from input
    private Reader reader ;
    // array of acceptors for the lexical classes, in order of priority
    private DFA[] acceptors ;
    // buffer to allow 1-token lookahead
    private Token bufferToken ;
    private boolean bufferInUse = false ;

    // Constructor
    public GenLexer (Reader reader, DFA[] acceptors) {
	    this.reader = reader ;
	    this.acceptors = acceptors ;
    }

    // Retrieves the next token
    private Token nextToken () throws LexError, StateOutOfRange, IOException {
        char c;                                         // current input character
        StringBuilder definite = new StringBuilder();   // characters up to last acceptance point
        StringBuilder maybe = new StringBuilder();      // characters since last acceptance point
        int acceptorIndex = -1;                         // array index of highest priority acceptor
        boolean liveFound;                              // flags for use in
        boolean acceptorFound;                          // iteration over acceptors

        // Reset acceptors states
        for (DFA acceptor : acceptors) {
            acceptor.reset();
        }

        do {
            c = (char)(reader.read()) ;
            acceptorFound = false ;
            liveFound = false ;
            if (c != EOF) {
                maybe.append(c);
                for (int i=0; i<acceptors.length; i++) {
                    acceptors[i].processChar(c) ;
                    if (!acceptors[i].isDead()) {
                        liveFound = true ;
                    }
                    if (!acceptorFound && acceptors[i].isAccepting()) {
                        acceptorFound = true ;
                        acceptorIndex = i ;
                        definite.append(maybe);
                        maybe = new StringBuilder();
                        reader.mark(10) ; // register backup point in input
                    }
                }
            }
            if (c == EOF) break;
        } while (liveFound) ;


        if (acceptorIndex >= 0) { // lex token has been found
            // backup to last acceptance point and output token
            reader.reset() ;
            String theLexClass = acceptors[acceptorIndex].lexClass() ;
            return new Token(definite.toString(), theLexClass) ;
        } else if (c == EOF && maybe.toString().equals("")) {
            // end of input already reached before nextToken was called
            reader.close() ;
            return null ;    // by convention, signifies end of input
        } else {
            reader.close() ;
            throw new LexError(maybe.toString()) ;
        }
    }


    // Used to peek next token
    public Token peekToken () throws LexError, StateOutOfRange, IOException {
        if (bufferInUse) {
            return bufferToken ;
        } else {
            bufferToken = nextToken() ;
            bufferInUse = true ;
            return bufferToken ;
        }
    }

    // Retrieves next token
    public Token pullToken () throws LexError, StateOutOfRange, IOException {
        peekToken () ;
        bufferInUse = false ;
        return bufferToken ;
    }


    public Token peekProperToken () throws LexError, StateOutOfRange, IOException {
        Token tok = peekToken () ;
        while (tok != null && tok.lexClass().equals("")) {
            pullToken () ;
            tok = peekToken () ;
        }
        bufferToken = tok ;
        bufferInUse = true ;
        return tok ;
    }

    public Token pullProperToken () throws LexError, StateOutOfRange, IOException {
        peekProperToken () ;
        bufferInUse = false ;
        return bufferToken ;
    }
}

