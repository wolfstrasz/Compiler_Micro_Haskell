public class Token {
    private final String value, lexClass ;
    Token(String value, String lexClass) {
        this.value = value ;
        this.lexClass = lexClass ;
    }
    public String value () {return this.value ;}
    public String lexClass () {return this.lexClass ;}
}

// Example: new Token ("5", "num")

