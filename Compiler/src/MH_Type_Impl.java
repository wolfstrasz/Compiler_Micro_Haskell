
interface MH_TYPE {
    boolean isInteger() ;
    boolean isBool() ;
    boolean isArrow() ;
    MH_TYPE left() ;    // returns left constituent of arrow type
    MH_TYPE right() ;   // returns right constituent
    boolean equals (MH_TYPE other) ;
    String toString() ; // for testing/debugging
}

public class MH_Type_Impl implements MH_TYPE {
    private int     kind;
    private MH_TYPE leftChild;
    private MH_TYPE rightChild;

    public boolean isInteger()  {return kind==0;}
    public boolean isBool()     {return kind==1;}
    public boolean isArrow()    {return kind==2;}
    public MH_TYPE left()       {return leftChild;}
    public MH_TYPE right()      {return rightChild;}

    public boolean equals (MH_TYPE other) {
        return ((this.isInteger() && other.isInteger()) ||
            (this.isBool() && other.isBool()) ||
            (this.isArrow() && other.isArrow() &&
             this.left().equals(other.left()) &&
             this.right().equals(other.right())));
    }

    public String toString () {
	if (this.isInteger()) return "Integer" ;
	else if (this.isBool()) return "Bool" ;
	else return ("(" + this.left().toString() + " -> "
		     + this.right().toString() + ")") ;
    }

    // Constructors
    private MH_Type_Impl (int kind, MH_TYPE leftChild, MH_TYPE rightChild) {
        this.kind = kind ;
        this.leftChild = leftChild ;
        this.rightChild = rightChild ;
    }

    // Constants for MH types Integer and Bool
    static MH_TYPE IntegerType = new MH_Type_Impl (0,null,null);
    static MH_TYPE BoolType = new MH_Type_Impl (1,null,null);
    // Constructor for arrow types
    MH_Type_Impl (MH_TYPE leftChild, MH_TYPE rightChild) {
	this (2, leftChild, rightChild) ;
    }

    // #Type    -> #Type1 #TypeOps
    // #Type1   -> Integer | Bool | ( #Type )
    // #TypeOps -> epsilon | -> #Type
    static MH_Parser MH_Parser1 = new MH_Parser() ;

    static MH_TYPE convertType (TREE tree) {
        if (tree.getChildren()[1].getRhs() == MH_Parser.epsilon){
            // Case if TypeOps is empty
            return convertType1 (tree.getChildren()[0]);
        } else {
            // Case if TypeOps is -> Type
            MH_TYPE left = convertType1 (tree.getChildren()[0]);
            MH_TYPE right = convertType(tree.getChildren()[1].getChildren()[1]);
            return new MH_Type_Impl (left,right);
        }
    }

    static MH_TYPE convertType1 (TREE tree1) {
        if (tree1.getRhs() == MH_Parser.Integer) {
            return MH_Type_Impl.IntegerType ;
        } else if (tree1.getRhs() == MH_Parser.Bool) {
            return MH_Type_Impl.BoolType ;
        } else // This covers case in which tree1 matches ( Type )
            return convertType (tree1.getChildren()[1]) ;
    }

}

