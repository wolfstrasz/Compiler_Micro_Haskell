package MyErrors;

public class DuplicatedVariable extends Exception {
    public DuplicatedVariable (String var) {
	super("Duplicated variable " + var);
    }
}
