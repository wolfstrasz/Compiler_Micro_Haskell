package MyErrors;

public class UnknownVariable extends Exception {
    public UnknownVariable (String var) {
	super("Variable " + var + " not in scope.");
    }
}
