package MyErrors;

public class UnknownSymbol extends Exception {
    public UnknownSymbol (String s) {
        super ("Unknown symbolic token " + s) ;
    }
}