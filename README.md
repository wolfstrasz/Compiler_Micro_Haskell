# Micro-Haskell Compiler 
First try of creating a compiler.
Old project that I did some refactoring on but found bugs that I am considering fixing sooner or later.
As it is not fully functional here is a link to my fully functional [Mini-C compiler]()
It was implemented in 4 parts:
* Lexer 
* Parser
* Typechecker (has errors)
* Evaluator (Should work but can't evaluate due to errors in Typechecker)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Java Development Kit v1.8 (JDK):
    - install from [Oracle website](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

### Compiling

1. Download the project.

2. Open a terminal in the Compiler folder (where the build file is).

3. Build using ant

```
>> ant build
```

## Running the tests
All application runs should be done from the terminal in Compiler folder.
There are 4 different instances that can be used for testing


# Lexer
The lexer demo by itself has a bit akward behaviour.

```
>> java -cp bin LexerDemo 
```

This should start the emulator where you can type in text that gets tokenized.
See the list of tokens in GrammarAndTokens.txt

```
Lexer> True
>> True BOOLEAN
Lexer> Integer
>> Integer Integer
Lexer> myVariable
>> myVariable VAR 
```

# Parser
Parser with lexer fully functional together. You can use own tests (replace test0.txt).

```
>> java -cp bin MH_ParserDermo test0.txt
```

# Typechecker
Problematic. Has errors and always does everything successfully even if it is not.

```
>> java -cp bin MH_Typechecker test0.txt
```

# Evaluator
Fails due to errors in typechecking
```
>> java -cp bin MH_Evaluator test0.txt
```

## Authors

* **Boyan Yotov**

## License

Feel free to use and distribute.
